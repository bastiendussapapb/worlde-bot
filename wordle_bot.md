# Solving Wordle with Python

This short project aims at creating an AI to cheat at [wordle](https://www.powerlanguage.co.uk/wordle/) using Python.


## Structure

* **wordle.ipynb** : The main markdown.
* **wordle.py**: contains the source code.
* **words_dictionary.json** : A dictionary containing all english words.
* **words_5_letters.json** : A dictionary containing all english words of 5 letters.

