import json
import string

def value_word(word, alphabet):
    tmp = 0
    for letter in set(word):
        tmp += alphabet[letter]
    return tmp

f = open("words_dictionary.json", "r")
all_words = json.loads(f.read())
words_5_letters_list = []
words_5_letters = dict()

for words in all_words.keys():
    if len(words) == 5:
        words_5_letters_list.append(words)

alphabet = dict.fromkeys(string.ascii_lowercase, 0)
for words in words_5_letters_list:
    for s in words:
        alphabet[s] += 1


for words in words_5_letters_list:
    words_5_letters[words] = value_word(words, alphabet)

with open('words_5_letters.json', 'w') as f:
    json.dump(words_5_letters, f)