from numpy.random import choice
from string import ascii_lowercase
from collections import defaultdict
import numpy as np


def play_wordle(dictionary, solution, random=False):
    """
    Play a game of wordle when the solution is given as input.
    """
    if len(solution) != 5:
        raise TypeError(f"Size of the words = {len(solution)}, should be 5") 
    
    if solution not in dictionary.keys():
        raise TypeError(f"The word {solution} is unknown")

    to_use      = []
    not_in      = []
    correct_pos = dict()
    wrong_pos   = defaultdict(list)

    if random:
        print("The super-sophisticated AI will guess with randomness.")
    else:
        print("The super-sophisticated AI will guess without randomness, only using facts and logic\n")
    for i in range(6):
        words = possible_words(
                dictionary, to_use, not_in, correct_pos, wrong_pos)
        if random:
            guess = choose_a_random_word(words)
        else:
            guess = list(words)[0]
        print(f"There is {len(list(words))} possiblities, the super-sophisticated AI uses it's super intelligence and tries : {guess}")
        if solution == guess:
            print(f"\nCORRECT\nIt only took {i+1} steps")
            break
        else:
            for letter in guess:
                if letter in solution and letter not in to_use:
                    to_use.append(letter)
                elif letter not in solution and letter not in not_in:
                    not_in.append(letter)
            
            for j in range(5):
                if guess[j] == solution[j]:
                    correct_pos[guess[j]] = j+1
                elif guess[j] in solution:
                    wrong_pos[guess[j]].append(j+1)

            print(f"Letters contained in the word : {to_use}")
            print(f"Letters not contained in the word : {not_in}")
            print(f"Letters and their known position : {correct_pos}")
            print(f"Letters and their known wrong position : {dict(wrong_pos)}\n")

    if guess != solution:
        print("The super-sophisticated AI failed ... \n")
        print(f"The solution was {solution}")
        print(
            f"List of remaining possible  words : {possible_words(dictionary, to_use, not_in, correct_pos, wrong_pos)}")

def possible_words(dictionary, to_use, not_in, correct_pos, wrong_pos):
    """
    Return all words that contains the letter "to_use", none of the letters of "not_in" 
    and that satisfies the location constrains in correct_pos and wrong_pos.

    dictionary : English dictionary, the values don't matter
    not_in : iterable
    placement : iterable
    placement : dict
    """

    tmp = dict()
    for w in dictionary.keys():
        if (all([s in w for s in to_use]) and 
            all([s not in w for s in not_in]) and 
            all([w[idx-1] == letter for letter, idx in correct_pos.items()]) and 
            all([all([w[idx-1] != letter for idx in idxs]) for letter, idxs in wrong_pos.items()])):
            tmp[w] = dictionary[w]

    return dict(sorted(tmp.items(), key=lambda x: x[1], reverse=True))

def choose_a_random_word(words):
    proba = np.array(list(words.values()))
    return choice(list(words.keys()), size=1, replace=True,p=proba/sum(proba)).item()
